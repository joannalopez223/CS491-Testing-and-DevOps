import React from 'react';

import UsersList from '../components/UsersList';

const Users = () => {
  const USERS = [
    {
      id: 'u1',
      name: 'Joanna Lopez',
      image:
        'https://github.com/joannalopez223/CS491-Testing-and-DevOps/blob/main/images/Model.JPG?raw=true',
      places: 2
    },
    
  ];

  return <UsersList items={USERS} />;
};

export default Users;
