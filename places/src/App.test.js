import { render, screen } from '@testing-library/react';
import React from 'react';
import UsersList from './user/components/UsersList';
import Users from './user/pages/Users';
import USERS from './user/pages/Users';

describe('Users name in array containing "Joanna Lopez."', () => {
  const expected = [{name: "Joanna Lopez"}];
  it('matches elements', () => {
    expect(true);
  });
});

describe('If places array is empty.', () => {
  const expected = [{place: "1"}];
  it('matches elements', () => {
    expect(expected.length !== 0);
  });
});
s
describe('Checks if users array is empty.', () => {
  it('matches elements', () => {
    expect(USERS.length !== 2);
  });
});
