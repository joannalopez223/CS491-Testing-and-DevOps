import React from 'react';
import { useParams } from 'react-router-dom';

import PlaceList from '../components/PlaceList';

const DUMMY_PLACES = [
  {
    id: 'p1',
    title: 'Statue of Liberty',
    description: 'The Statue of Liberty Enlightening the World" was a gift of friendship from the people of France to the United States and is recognized as a universal symbol of freedom and democracy!',
    imageUrl:
      'https://github.com/joannalopez223/CS491-Testing-and-DevOps/blob/main/images/StatueLibery.JPG?raw=true',
    address: 'New York, NY 10004',
    location: {
      lat: 40.689247,
      lng: -74.044502
    },
    creator: 'u1'
  },
  {
    id: 'p2',
    title: 'Carmel by the Sea',
    description: 'Carmel-by-the-Sea is a small beach city on California Monterey Peninsula. Its known for the museums and library of the historic Carmel Mission, and the fairytale cottages and galleries of its village-like center!',
    imageUrl:
      'https://github.com/joannalopez223/CS491-Testing-and-DevOps/blob/main/images/Snow.JPG?raw=true',
    address: 'Carmel Beach, Carmel-By-The-Sea, CA 93923',
    location: {
      lat: 36.5550,
      lng: -121.9306
    },
    creator: 'u2'
  },
];

const UserPlaces = () => {
  const userId = useParams().userId;
  const loadedPlaces = DUMMY_PLACES.filter(place => place.creator === userId);
  return <PlaceList items={loadedPlaces} />;
};

export default UserPlaces;
